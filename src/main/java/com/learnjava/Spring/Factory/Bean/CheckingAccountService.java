package com.learnjava.Spring.Factory.Bean;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

@Service
public class CheckingAccountService implements AccountService, RegisterBean<AccountType>{

	@Override
	public AccountType getAccountType() {
		return AccountType.CHECKING;
	}

	@Override
	public BigDecimal getInterestRate() {
		// TODO Auto-generated method stub
		return BigDecimal.ZERO;
	}

	@Override
	public AccountType getRegisterId() {
		return AccountType.CHECKING;
	}

}
