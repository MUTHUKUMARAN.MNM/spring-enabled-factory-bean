package com.learnjava.Spring.Factory.Bean;

public enum AccountType {

		SAVINGS, CHECKING;
}
