package com.learnjava.Spring.Factory.Bean;

import java.math.BigDecimal;

public interface AccountService {
	
	AccountType getAccountType();
	
	BigDecimal getInterestRate();

}
