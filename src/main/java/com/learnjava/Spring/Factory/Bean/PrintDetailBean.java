package com.learnjava.Spring.Factory.Bean;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrintDetailBean {
	
	@Autowired
	AccountServiceFactory accountServiceFactory;
	
	@PostConstruct
	public void printAccountDetail() {
		
		System.out.println("Checking Account Details...");
		AccountService accountService = accountServiceFactory.getInstance(AccountType.CHECKING);
		System.out.println("Account Type : "+accountService.getAccountType());
		System.out.println("Account Type Interest Rate : "+accountService.getInterestRate());
		
		System.out.println("Savings Account Details...");
		accountService = accountServiceFactory.getInstance(AccountType.SAVINGS);
		System.out.println("Account Type : "+accountService.getAccountType());
		System.out.println("Account Type Interest Rate : "+accountService.getInterestRate());
	}

}
