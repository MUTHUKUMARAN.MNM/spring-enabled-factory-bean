package com.learnjava.Spring.Factory.Bean;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * <b>ServiceFactory</b> is the base class of all service factories. It has
 * default implementation to look up all matching service implementations based
 * on the supplied <K,V> pair and populates instance map of the corresponding service factory.
 * 
 * @author MuthukumaranN
 *
 * @param <K>
 * @param <V>
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
public abstract class ServiceFactory<K, V> implements InitializingBean, ApplicationContextAware {

	private static final Logger LOG = LoggerFactory.getLogger(ServiceFactory.class);

	private HashMap<K, V> instanceMap = new HashMap<>();

	ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		applicationContext = context;
	}

	public V getInstance(K key) {
		if (instanceMap.containsKey(key))
			return instanceMap.get(key);
		else
			throw new IllegalArgumentException("No service instance found for key {" + key + "} in {"
					+ getClass().getTypeName()
					+ "}. Please check if the expected service implementation has implemented RegisterBean interface with valid key.");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		Type type = getClass().getGenericSuperclass();
		Type keyType = ((ParameterizedType) type).getActualTypeArguments()[0];
		Type valueType = ((ParameterizedType) type).getActualTypeArguments()[1];
		if (keyType == null)
			throw new ClassNotFoundException("Invalid keyType : " + keyType);
		if (valueType == null)
			throw new ClassNotFoundException("Invalid valueType : " + valueType);
		Map<String, ?> beanMap = applicationContext.getBeansOfType(Class.forName((valueType).getTypeName()));
		if (beanMap == null || beanMap.size() == 0)
			LOG.warn("No Implementation found for : " + Class.forName((valueType).getTypeName()));
		beanMap.entrySet().stream().forEach(entry -> {
			V value = (V) entry.getValue();
			if (value instanceof RegisterBean) {
				RegisterBean<K> registerBean = (RegisterBean<K>) value;
				try {
					if (!Class.forName((keyType).getTypeName()).isInstance(registerBean.getRegisterId())) {
						throw new ClassCastException("Invalid Key {" + registerBean.getRegisterId() + "} of Type : "
								+ registerBean.getRegisterId().getClass().getTypeName() + " found in {"
								+ registerBean.getClass().getTypeName() + "},but expected of Type : "
								+ (keyType).getTypeName());
					}
				} catch (ClassNotFoundException e) {
					LOG.error("Unable to validate if the Key Type is valid.", e);
					throw new ClassCastException("Unable to validate if the Key Type is valid.");
				}
				K registerId = registerBean.getRegisterId();
				if (registerId == null)
					throw new IllegalArgumentException("Invalid Key {" + registerId + "} found in {"
							+ registerBean.getClass().getTypeName() + "}.");
				if (!instanceMap.containsKey(registerId)) {
					LOG.info("Adding Register Bean {{}} with Key {{}} to instance map of service factory {{}}",
							registerBean.getClass().getTypeName(), registerId, getClass().getTypeName());
					instanceMap.put(registerId, value);
				} else {
					LOG.error("Duplicate Register Bean Key {} found. Please check.", registerId);
					throw new IllegalArgumentException("Duplicate Register Bean Key found : " + registerId);
				}
			}
		});
		if (instanceMap.isEmpty())
			LOG.warn(
					"No Service Bean found for Factory {{}}. Please make sure the expected service implmentation has implmented interface {{}} with valid key.",
					getClass().getTypeName(), RegisterBean.class);
		LOG.debug("For Factory {{}}, following Service Bean(s) have been added :\n {{}}", getClass().getTypeName(),
				instanceMap);
	}

}
