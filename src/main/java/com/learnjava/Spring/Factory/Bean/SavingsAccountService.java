package com.learnjava.Spring.Factory.Bean;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

@Service
public class SavingsAccountService implements AccountService, RegisterBean<AccountType>{

	@Override
	public AccountType getAccountType() {
		return AccountType.SAVINGS;
	}

	@Override
	public BigDecimal getInterestRate() {
		return BigDecimal.ONE;
	}

	@Override
	public AccountType getRegisterId() {
		return AccountType.SAVINGS;
	}

}
