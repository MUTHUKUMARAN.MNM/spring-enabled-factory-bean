package com.learnjava.Spring.Factory.Bean;

/**
 * Interface to be implemented by beans that need to be added to the
 * corresponding service factory. Must register with key to be looked up.
 * 
 * @author MuthukumaranN
 *
 * @param <K>
 */
public interface RegisterBean<K> {

	/**
	 * Returns the register bean key. Key must be unique within implementations of the service interface.
	 * 
	 * @return
	 */
	public K getRegisterId();

}
